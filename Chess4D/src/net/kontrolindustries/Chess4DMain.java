package net.kontrolindustries;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Chess4DMain implements ApplicationListener {
	private SpriteBatch batch;
	private static Texture instructionsTex;
	
	public static Board chessboard;
	public static float camX, camY, zoom;
	public static OrthographicCamera camera;

	@Override
	public void create() {		
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		
		camera = new OrthographicCamera(w, h);
		batch = new SpriteBatch();
		
		Gdx.input.setInputProcessor(new ChessInputProcessor());
		
		camX = 320;
		camY = -10000;
		zoom = 0;
		
		chessboard = new Board();
		instructionsTex = new Texture(Gdx.files.internal("instructions.png"));
	}

	@Override
	public void dispose() {
		batch.dispose();
		chessboard.dispose();
	}

//	private int renderLoopNumber = 0;
	@Override
	public void render() {		
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		keepCameraOnScreen();
		
		batch.setProjectionMatrix(camera.combined);
		
		camera.translate(camX, camY, zoom);
		camX = 0;
		camY = 0;
		zoom = 0;

		batch.begin();
		chessboard.render(batch);
		batch.draw(instructionsTex, 0, -320);
		batch.end();
		
		camera.update();
//		if(renderLoopNumber > 100){
//			Gdx.app.log("FPS", Gdx.graphics.getFramesPerSecond()+"");
//			renderLoopNumber = 0;
//		}
//		renderLoopNumber++;
	}
	
	private void keepCameraOnScreen(){
		if(camera.position.x < 320)
			camera.position.x = 320;
		if(camera.position.y < 0)
			camera.position.y = 0;
		if(camera.position.x > 960)
			camera.position.x = 960;
		if(camera.position.y > 960)
			camera.position.y = 960;
	}

	@Override
	public void resize(int width, int height) {
		camera = new OrthographicCamera(width, height);
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}
