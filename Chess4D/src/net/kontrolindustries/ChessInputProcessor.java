package net.kontrolindustries;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;

public class ChessInputProcessor implements InputProcessor {
	private int lastXDragged = 0;
	private int lastYDragged = 0;

	@Override
	public boolean keyDown (int keycode) {
		if(keycode == Keys.ESCAPE){
			Gdx.app.exit();
		}
		return false;
	}

	@Override
	public boolean keyUp (int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped (char character) {
		return false;
	}

	@Override
	public boolean touchDown (int xTouched, int yTouched, int pointer, int button) {
		if(Gdx.app.getType() == ApplicationType.Android){
			if(pointer != 0){
				return false;
			}
		} else{
			if(button != Input.Buttons.LEFT){
				return false;
			}
		}
		Vector touched = new Vector();

		int x = xTouched;
		int y = (yTouched-Gdx.graphics.getHeight())*-1;

		x += Chess4DMain.camera.position.x - (Gdx.graphics.getWidth()/2);
		y += Chess4DMain.camera.position.y - (Gdx.graphics.getHeight()/2);

		touched.w = x/160;
		touched.z = y/160;

		touched.x = (x%160)/16-1;
		touched.y = (y%160)/16-1;

		if(Chess4DMain.chessboard.isMoving){
			Chess4DMain.chessboard.moveSelectedPiece(touched);
		}

		Chess4DMain.chessboard.selectPieceAt(touched);

		return false;
	}

	@Override
	public boolean touchUp (int x, int y, int pointer, int button) {
		lastXDragged = 0;
		lastYDragged = 0;
		return false;
	}

	@Override
	public boolean touchDragged (int x, int y, int pointer) {
		if(pointer == 1 || Gdx.input.isButtonPressed(Input.Buttons.RIGHT)){
			if(lastXDragged != 0)
				Chess4DMain.camX = (x-lastXDragged);
			if(lastYDragged != 0)
				Chess4DMain.camY = (y-lastYDragged);
			Chess4DMain.camX *= -1;
			lastXDragged = x;
			lastYDragged = y;
		}
		return false;
	}

	@Override
	public boolean scrolled (int amount) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}
}