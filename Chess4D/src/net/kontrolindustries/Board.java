package net.kontrolindustries;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import net.kontrolindustries.piece.*;

public class Board {

	private static Texture boardTex = new Texture(Gdx.files.internal("board.png"));
	private static Texture selectTex = new Texture(Gdx.files.internal("selection.png"));
	private static Texture moveToTex = new Texture(Gdx.files.internal("movetoselection.png"));

	private static Texture gameOverTexWhite = new Texture(Gdx.files.internal("whitewin.png"));
	private static Texture gameOverTexBlack = new Texture(Gdx.files.internal("blackwin.png"));

	private static Texture wxAxisRef = new Texture(Gdx.files.internal("wxaxisref.png"));
	private static Texture zyAxisRef = new Texture(Gdx.files.internal("zyaxisref.png"));

	private ArrayList<ChessPiece> pieces;
	private int pieceIndexSelected;
	private ArrayList<Vector> moveVectors;
	private boolean isGameOver;
	private boolean didWhiteWin;

	public boolean isWhiteTurn;
	public boolean isMoving;

	public Board(){
		pieces = new ArrayList<ChessPiece>();
		setupBoard();
		pieceIndexSelected = 0;
		isWhiteTurn = true;
		isGameOver = false;
	}
	private void setupBoard(){
		for(int i = 0; i < 8; i++){
			pieces.add(new Pawn(true, new Vector(0,i,1,0)));
			pieces.add(new Pawn(true, new Vector(0,i,0,1)));
			pieces.add(new Pawn(true, new Vector(1,i,0,0)));

			pieces.add(new Pawn(false, new Vector(7,i,6,7)));
			pieces.add(new Pawn(false, new Vector(7,i,7,6)));
			pieces.add(new Pawn(false, new Vector(6,i,7,7)));
		}

		pieces.add(new Rook(true, new Vector(0,0,0,0)));
		pieces.add(new Rook(true, new Vector(0,7,0,0)));
		pieces.add(new Rook(false, new Vector(7,0,7,7)));
		pieces.add(new Rook(false, new Vector(7,7,7,7)));
		/////////////////////////////////////////////////////
		pieces.add(new Knight(false, new Vector(7,1,7,7)));
		pieces.add(new Knight(false, new Vector(7,6,7,7)));
		pieces.add(new Knight(true, new Vector(0,1,0,0)));
		pieces.add(new Knight(true, new Vector(0,6,0,0)));
		/////////////////////////////////////////////////////
		pieces.add(new Bishop(false, new Vector(7,2,7,7)));
		pieces.add(new Bishop(false, new Vector(7,5,7,7)));
		pieces.add(new Bishop(true, new Vector(0,2,0,0)));
		pieces.add(new Bishop(true, new Vector(0,5,0,0)));
		/////////////////////////////////////////////////////
		pieces.add(new King(true, new Vector(0,4,0,0)));
		pieces.add(new Queen(true, new Vector(0,3,0,0)));
		pieces.add(new King(false, new Vector(7,4,7,7)));
		pieces.add(new Queen(false, new Vector(7,3,7,7)));
	}

	/**
	 * Used to get the piece at a specific coordinate.  However,
	 * it will return null if nothing is at that coordinate.
	 * @param position A specific coordinate
	 * @return The piece at that coordinate, null if nothing is there or off the boaard
	 */
	public ChessPiece getSpace(Vector position) {
		if(!position.isValid())
			return null;
		for(int i = 0; i < pieces.size(); i++){
			if(pieces.get(i).getPosition().equals(position)){
				return pieces.get(i);
			}
		}
		return null;
	}
	/**
	 * Removes the piece at a specific position
	 * @param position The piece's position to remove
	 */
	public void removePieceAt(Vector position){
		for(int i = 0; i < pieces.size(); i++){
			if(pieces.get(i).getPosition().equals(position)){
				if(pieces.get(i) instanceof King){
					isGameOver = true;
					didWhiteWin = !pieces.get(i).isWhite();
				}
				pieces.get(i).dispose();
				pieces.remove(i);
			}
		}
	}

	/**
	 * Used to get whether or not the space at
	 * a position is white or not.  Will return
	 * null if the space is unoccupied.
	 * @param vec The position to check
	 * @return Whether or not the piece is white at vec or null if unoccupied
	 * @throws VectorOOBException
	 */
	public Boolean isSpaceOccupiedByWhite(Vector vec) throws VectorOOBException{
		if(!vec.isValid())
			throw new VectorOOBException();
		ChessPiece piece = null;
		piece = getSpace(vec);
		if(piece == null)
			return null;
		return piece.isWhite();
	}

	/**
	 * Used to get whether or not a piece
	 * exists at vec position
	 * @param vec The position to check
	 * @return Whether or not a piece exists at vec
	 * @throws VectorOOBException
	 */
	public boolean isSpaceOccupied(Vector vec) throws VectorOOBException{
		if(!vec.isValid())
			throw new VectorOOBException();
		return getSpace(vec) != null;
	}

	/**
	 * Render the board and all its pieces 
	 * to the screen.  Should only be called in
	 * the render() loop.
	 * @param batch The SpriteBatch to render with
	 */
	public void render(SpriteBatch batch){
		//The board...
		for(int x = 0; x < 8; x++){
			for(int y = 0; y < 8; y++){
				batch.draw(boardTex, x*boardTex.getWidth(), y*boardTex.getHeight());
			}
		}
		batch.draw(wxAxisRef, 0, -160);
		batch.draw(zyAxisRef, -160, 0);

		//The pieces...
		for(int i = 0; i < pieces.size(); i++){
			int x = pieces.get(i).getPosition().x*16;
			int y = pieces.get(i).getPosition().y*16;
			x += 16;
			y += 16;
			x += pieces.get(i).getPosition().w * 160;
			y += pieces.get(i).getPosition().z * 160;
			batch.draw(pieces.get(i).getPieceTex(), x, y);
		}

		//Other...
		if(!isGameOver){
			if(isMoving && moveVectors.size() > 0 && pieceIndexSelected >= 0){
				for(int i = 0; i < moveVectors.size(); i++){
					int x = moveVectors.get(i).x*16;
					int y = moveVectors.get(i).y*16;
					x += 16;
					y += 16;
					x += moveVectors.get(i).w * 160;
					y += moveVectors.get(i).z * 160;
					batch.draw(moveToTex, x, y);
					x = pieces.get(pieceIndexSelected).getPosition().x*16;
					y = pieces.get(pieceIndexSelected).getPosition().y*16;
					x += 16;
					y += 16;
					x += pieces.get(pieceIndexSelected).getPosition().w * 160;
					y += pieces.get(pieceIndexSelected).getPosition().z * 160;
					batch.draw(selectTex, x, y);
				}
			}
		} else{
			int x = (int) Chess4DMain.camera.position.x-Gdx.graphics.getWidth()/2;
			int y = (int) Chess4DMain.camera.position.y-Gdx.graphics.getHeight()/2;
			if(didWhiteWin){
				batch.draw(gameOverTexWhite, x, y);
			} else{
				batch.draw(gameOverTexBlack, x, y);
			}
		}
	}
	public void deselectPiece(){
		moveVectors = null;
		isMoving = false;
	}

	public void selectPieceAt(Vector vec){
		if(vec == null || !vec.isValid()){
			isMoving = false;
			return;
		}
		ChessPiece tmpPiece = null;
		for(int i = 0; i < pieces.size(); i++){
			if(pieces.get(i).getPosition().equals(vec)){
				tmpPiece = pieces.get(i);
				pieceIndexSelected = i;
				break;
			}
		}

		if(tmpPiece == null || isWhiteTurn != pieces.get(pieceIndexSelected).isWhite()){
			pieceIndexSelected = -1;
			return;
		}

		moveVectors = tmpPiece.getLegalMoves(this);

		if(moveVectors.size() < 1){
			pieceIndexSelected = -1;
			return;
		}

		isMoving = true;
	}

	public void deselect(){
		pieceIndexSelected = -1;
	}

	public void moveSelectedPiece(Vector vec){
		if(isGameOver || !vec.isValid() || pieceIndexSelected < 0){
			return;
		}
		ChessPiece selectedPiece = pieces.get(pieceIndexSelected);

		if(moveVectors.isEmpty()){
			return;
		}

		boolean vecIsValid = false;
		for(int i = 0; i < moveVectors.size(); i++){
			if(vec.equals(moveVectors.get(i))){
				vecIsValid = true;
				break;
			}
		}
		if(!vecIsValid)
			return;

		try {
			if(isSpaceOccupiedByWhite(vec) != null){
				if(isSpaceOccupiedByWhite(vec) != selectedPiece.isWhite()){
					removePieceAt(vec);
				}
			}
		} catch (VectorOOBException e) {}
		selectedPiece.move(vec);

		if(selectedPiece instanceof Pawn){
			Pawn tmp = (Pawn)selectedPiece;
			if(tmp.isToBePromoted()){
				removePieceAt(tmp.getPosition());
				pieces.add(new Queen(true, tmp.getPosition()));
			}
		}
		isWhiteTurn = !isWhiteTurn;

		isMoving = false;
		deselect();
	}

	public void dispose(){
		boardTex.dispose();
		moveToTex.dispose();
		selectTex.dispose();
		wxAxisRef.dispose();
		zyAxisRef.dispose();
		for(int i = 0; i < pieces.size(); i++){
			pieces.get(i).dispose();
		}
	}
}
