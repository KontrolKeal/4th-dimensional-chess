package net.kontrolindustries;

public class Vector {
	public static int[][] perms1D = {
		{1,0,0,0}, {0,1,0,0}, {0,0,1,0}, {0,0,0,1},
		{-1,0,0,0}, {0,-1,0,0}, {0,0,-1,0}, {0,0,0,-1}
	};
	public static int[][] perms2D = {
		{1,1,0,0}, {0,1,1,0}, {0,0,1,1}, {1,0,0,1}, {0,1,0,1}, {1,0,1,0},
		{-1,1,0,0}, {0,-1,1,0}, {0,0,-1,1}, {-1,0,0,1}, {0,-1,0,1}, {-1,0,1,0},
		{1,-1,0,0}, {0,1,-1,0}, {0,0,1,-1}, {1,0,0,-1}, {0,1,0,-1}, {1,0,-1,0},
		{-1,-1,0,0}, {0,-1,-1,0}, {0,0,-1,-1}, {-1,0,0,-1}, {0,-1,0,-1}, {-1,0,-1,0}
	};
	
	int w, x, y, z;
	
	/**
	 * Represents a vector in 4 dimensions with
	 * the given parameters.
	 * @param w The W coordinate
	 * @param x The X coordinate
	 * @param y The Y coordinate
	 * @param z The Z coordinate
	 */
	public Vector(int w, int x, int y, int z){
		this.w = w;
		this.x = x;
		this.y = y;
		this.z = z;
	}
	/**
	 * Creates a Vector representing (0, 0, 0, 0)
	 * in 4 dimensional coordinates.
	 */
	public Vector(){
		this(0, 0, 0, 0);
	}
	
	/**
	 * Will check if the piece is actually valid on the board
	 * @return
	 */
	public boolean isValid() {
		return  (w < 8 && w >= 0) && 
				(x < 8 && x >= 0) && 
				(y < 8 && y >= 0) && 
				(z < 8 && z >= 0);
	}
	
	public static Vector add(Vector vec1, Vector vec2){
		return new Vector(
				vec1.w + vec2.w,
				vec1.x + vec2.x,
				vec1.y + vec2.y,
				vec1.z + vec2.z );
	}
	
	public static Vector multiply(Vector vec1, Vector vec2){
		return new Vector(
				vec1.w * vec2.w,
				vec1.x * vec2.x,
				vec1.y * vec2.y,
				vec1.z * vec2.z );
	}
	
	public static double distanceBetween(Vector vec1, Vector vec2){
		return Math.sqrt(
				Math.pow(vec1.w,vec2.w) + 
				Math.pow(vec1.x,vec2.x) + 
				Math.pow(vec1.y,vec2.y) + 
				Math.pow(vec1.z,vec2.z)
				);
	}
	
	public boolean equals(Vector vec){
		return (vec != null) && (vec.w == w) && (vec.x == x) && (vec.y == y) && (vec.z == z);
	}
	
	public String toString(){
		return "("+w+", "+x+", "+y+", "+z+")";
	}
}
