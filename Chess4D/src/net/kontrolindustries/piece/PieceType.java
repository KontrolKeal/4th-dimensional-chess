package net.kontrolindustries.piece;

public enum PieceType {
	PAWN,
	ROOK,
	KNIGHT,
	BISHOP,
	KING,
	QUEEN;
}
