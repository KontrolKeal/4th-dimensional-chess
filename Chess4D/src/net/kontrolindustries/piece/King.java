package net.kontrolindustries.piece;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

import net.kontrolindustries.Board;
import net.kontrolindustries.Vector;
import net.kontrolindustries.VectorOOBException;

public class King extends ChessPiece {

	public King(boolean isWhite, Vector position) {
		super(isWhite, position, PieceType.KING);
		if(isWhite){
			pieceTex = new Texture(Gdx.files.internal("pieces/King1.png"));
		} else{
			pieceTex = new Texture(Gdx.files.internal("pieces/King2.png"));
		}
	}

	@Override
	public ArrayList<Vector> getLegalMoves(Board board) {
		ArrayList<Vector> legalMoves = new ArrayList<Vector>();

		for(int p = 0; p < Vector.perms1D.length; p++){
			Vector testVec = Vector.add(position, new Vector(Vector.perms1D[p][0],Vector.perms1D[p][1],Vector.perms1D[p][2],Vector.perms1D[p][3]));
			try {
				if(board.isSpaceOccupiedByWhite(testVec) == null || board.isSpaceOccupiedByWhite(testVec) != isWhite){
					legalMoves.add(testVec);
				}
			} catch (VectorOOBException e) {
			}
		}
		for(int p = 0; p < Vector.perms2D.length; p++){
			Vector testVec = Vector.add(position, new Vector(Vector.perms2D[p][0],Vector.perms2D[p][1],Vector.perms2D[p][2],Vector.perms2D[p][3]));
			try {
				if(board.isSpaceOccupiedByWhite(testVec) == null || board.isSpaceOccupiedByWhite(testVec) != isWhite){
					legalMoves.add(testVec);
				}
			} catch (VectorOOBException e) {
			}
		}
		return legalMoves;
	}
}
