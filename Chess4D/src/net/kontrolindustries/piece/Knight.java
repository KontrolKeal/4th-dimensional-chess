package net.kontrolindustries.piece;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

import net.kontrolindustries.Board;
import net.kontrolindustries.Vector;
import net.kontrolindustries.VectorOOBException;

public class Knight extends ChessPiece {
	public Knight(boolean isWhite, Vector position) {
		super(isWhite, position, PieceType.KNIGHT);
		if(isWhite){
			pieceTex = new Texture(Gdx.files.internal("pieces/Knight1.png"));
		} else{
			pieceTex = new Texture(Gdx.files.internal("pieces/Knight2.png"));
		}
	}

	@Override
	public ArrayList<Vector> getLegalMoves(Board board) {
		ArrayList<Vector> legalMoves = new ArrayList<Vector>();
		for(int w = -2; w <= 2; w++){
			for(int x = -2; x <= 2; x++){
				for(int y = -2; y <= 2; y++){
					for(int z = -2; z <= 2; z++){
						Vector testVec = Vector.add(position, new Vector(w, x, y, z));
						if(testVec.isValid() && (w*w + x*x + y*y + z*z) == 5){
							try {
								if(board.isSpaceOccupiedByWhite(testVec) == null ){
									legalMoves.add(testVec);
								} else if(board.isSpaceOccupiedByWhite(testVec) != isWhite){
									legalMoves.add(testVec);
								}
							} catch (VectorOOBException e) {
							}
						}
					}
				}
			}
		}
		return legalMoves;
	}
}
