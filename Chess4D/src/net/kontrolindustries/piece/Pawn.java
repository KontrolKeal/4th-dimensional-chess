package net.kontrolindustries.piece;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

import net.kontrolindustries.Board;
import net.kontrolindustries.Vector;
import net.kontrolindustries.VectorOOBException;

public class Pawn extends ChessPiece {
	private static final Vector[] FORWARDS_WHITE = new Vector[]{new Vector( 0, 0, 1, 0), new Vector( 1, 0, 0, 0), new Vector( 0, 0, 0, 1)};
	private static final Vector[] FORWARDS_BLACK = new Vector[]{new Vector( 0, 0,-1, 0), new Vector(-1, 0, 0, 0), new Vector( 0, 0, 0,-1)};

	private static final Vector[] CAPTURES_WHITE = new Vector[]{
		new Vector( 0, 0, 1, 0),
		new Vector( 0, 1, 1, 0),
		new Vector( 0,-1, 1, 0),
		new Vector( 1, 0, 0, 0),
		new Vector( 1, 1, 0, 0),
		new Vector( 1,-1, 0, 0),
		new Vector( 0, 0, 0, 1),
		new Vector( 0, 1, 0, 1),
		new Vector( 0,-1, 0, 1)
	};

	private static final Vector[] CAPTURES_BLACK = new Vector[]{
		new Vector( 0, 0,-1, 0),
		new Vector( 0, 1,-1, 0),
		new Vector( 0,-1,-1, 0),
		new Vector(-1, 0, 0, 0),
		new Vector(-1, 1, 0, 0),
		new Vector(-1,-1, 0, 0),
		new Vector( 0, 0, 0,-1),
		new Vector( 0, 1, 0,-1),
		new Vector( 0,-1, 0,-1)
	};

	private boolean isFirstMove;
	private final Vector startPosition;

	public Pawn(boolean isWhite, Vector position) {
		super(isWhite, position, PieceType.PAWN);
		if(isWhite){
			pieceTex = new Texture(Gdx.files.internal("pieces/Pawn1.png"));
		} else{
			pieceTex = new Texture(Gdx.files.internal("pieces/Pawn2.png"));
		}
		isFirstMove = true;
		startPosition = position;
	}

	@Override
	public ArrayList<Vector> getLegalMoves(Board board) {
		ArrayList<Vector> legalMoves = new ArrayList<Vector>();
		Vector[] vectorsToTest = null;

		if(!position.equals(startPosition)){
			isFirstMove = false;
		}
		if(isWhite){
			vectorsToTest = FORWARDS_WHITE;
		} else{
			vectorsToTest = FORWARDS_BLACK;
		}
		if(!isFirstMove){
			for(int i = 0; i < vectorsToTest.length; i++){
				Vector testVec = Vector.add(position, vectorsToTest[i]);
				try {
					if(testVec.isValid() && board.isSpaceOccupiedByWhite(testVec) == null){
						legalMoves.add(testVec);
					}
				} catch (VectorOOBException e) {
				}
			}
		} else{
			for(int c = 1; c <= 2; c++){
				for(int i = 0; i < vectorsToTest.length; i++){
					Vector testVec = Vector.add(position, Vector.multiply(vectorsToTest[i], new Vector(c, c, c, c)));
					try {
						if(testVec.isValid() && board.isSpaceOccupiedByWhite(testVec) == null){
							legalMoves.add(testVec);
						}
					} catch (VectorOOBException e) {
					}
				}
			}
		}

		if(isWhite){
			vectorsToTest = CAPTURES_WHITE;
		} else{
			vectorsToTest = CAPTURES_BLACK;
		}

		for(int i = 0; i < vectorsToTest.length; i++){
			Vector testVec = Vector.add(position, vectorsToTest[i]);
			try {
				if(testVec.isValid() && board.isSpaceOccupiedByWhite(testVec) != null && board.isSpaceOccupiedByWhite(testVec) != isWhite){
					legalMoves.add(testVec);
				}
			} catch (VectorOOBException e) {
			}
		}

		return legalMoves;
	}

	/**
	 * Used to figure out if this pawn
	 * should be promoted based on its
	 * current position.
	 * @return Whether or not to be promoted
	 */
	public boolean isToBePromoted(){
		boolean promote = true;
		Vector[] forwardVectors = null;

		if(isWhite){
			forwardVectors = FORWARDS_WHITE;
		} else{
			forwardVectors = FORWARDS_BLACK;
		}

		for(int i = 0; i < forwardVectors.length; i++){
			Vector testVec = Vector.add(position, forwardVectors[i]);
			if(testVec.isValid()){
				promote = false;
			}
			if(!promote){
				break;
			}
		}

		return promote;
	}
}
