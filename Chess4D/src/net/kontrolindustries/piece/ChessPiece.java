package net.kontrolindustries.piece;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.Texture;

import net.kontrolindustries.Board;
import net.kontrolindustries.Vector;

public abstract class ChessPiece {
	protected Texture pieceTex;
	
	protected Vector position;
	
	protected boolean isWhite;
	protected PieceType pieceType;
	
	public ChessPiece(boolean isWhite, Vector position, PieceType pieceType) {
		this.isWhite = isWhite;
		this.position = position;
		this.pieceType = pieceType;
	}
	
	/**
	 * Moves this piece to this position.  If position
	 * is null then don't do anything
	 * @param position A position to move to
	 */
	public void move(Vector position){
		if(position == null){
			return;
		}
		this.position = position;
	}
	
	/**
	 * Getter to get the current position
	 * of this piece
	 * @return This piece's position
	 */
	public Vector getPosition(){
		return position;
	}
	
	/**
	 * Returns whether or not the piece is white
	 * @return true if this piece is white
	 */
	public boolean isWhite(){
		return isWhite;
	}
	
	/**
	 * Getter for the piece's type
	 * @return This piece's type
	 */
	public PieceType getPieceType(){
		return pieceType;
	}
	
	/**
	 * Getter for the piece's texture
	 * @return This piece's Texture
	 */
	public Texture getPieceTex(){
		return pieceTex;
	}
	
	/**
	 * Returns an ArrayList of Vectors showing
	 * all possible moves for the piece.
	 * @return All possible moves for the piece
	 */
	public abstract ArrayList<Vector> getLegalMoves(Board board);
	
	/**
	 * Used to dispose of all resources in
	 * LibGDX that require disposal
	 */
	public void dispose(){
		pieceTex.dispose();
	}
}
