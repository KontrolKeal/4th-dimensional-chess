package net.kontrolindustries.piece;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

import net.kontrolindustries.Board;
import net.kontrolindustries.Vector;
import net.kontrolindustries.VectorOOBException;

public class Bishop extends ChessPiece {
	public Bishop(boolean isWhite, Vector position){
		super(isWhite, position, PieceType.BISHOP);
		if(isWhite){
			pieceTex = new Texture(Gdx.files.internal("pieces/Bishop1.png"));
		} else{
			pieceTex = new Texture(Gdx.files.internal("pieces/Bishop2.png"));
		}
	}

	@Override
	public ArrayList<Vector> getLegalMoves(Board board) {
		ArrayList<Vector> legalMoves = new ArrayList<Vector>();
		boolean isInterrupted = false;
		for(int p = 0; p < Vector.perms2D.length; p++){
			for(int i = 1; !isInterrupted; i++){
				Vector testVec = Vector.add(position, new Vector(i*Vector.perms2D[p][0],i*Vector.perms2D[p][1],i*Vector.perms2D[p][2],i*Vector.perms2D[p][3]));
				try {
					if(board.isSpaceOccupiedByWhite(testVec) == null){
						legalMoves.add(testVec);
					} else if(board.isSpaceOccupiedByWhite(testVec) != isWhite){
						legalMoves.add(testVec);
						isInterrupted = true;			
					}
					else{
						isInterrupted = true;
					}
				} catch (VectorOOBException e) {
					isInterrupted = true;
				}
			}
			isInterrupted = false;
		}
		return legalMoves;
	}
}
